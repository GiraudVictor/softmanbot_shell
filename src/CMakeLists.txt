cmake_minimum_required(VERSION 3.0.2)
project(softmanbot)

## Compile as C++11, supported in ROS Kinetic and newer
add_compile_options(-std=c++11)

find_package(catkin REQUIRED COMPONENTS 
	roscpp
	rospy
	eigen_conversions
	geometry_msgs
	roscpp
	rospy
	sensor_msgs
	std_msgs
	tf
	tf_conversions
	visualization_msgs
	message_generation
)

include(perception/perceptionCMake1.txt)
include(deformationControl/deformationControlCMake1.txt)
include(supervisor/supervisorCMake1.txt)
include(skilledWorkcell/skilledWorkcellCMake1.txt)

catkin_package(
#  INCLUDE_DIRS include
#  LIBRARIES softmanbot_ros
#  CATKIN_DEPENDS campero_ur_ip_controllers
#  DEPENDS system_lib
)

include(perception/perceptionCMake2.txt)
include(deformationControl/deformationControlCMake2.txt)
include(supervisor/supervisorCMake2.txt)
include(skilledWorkcell/skilledWorkcellCMake2.txt)


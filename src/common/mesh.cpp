#include "mesh.hpp"
#include <string>

#include <iostream>
#include <sstream>

mesh::mesh()
{
	node n1(0, 1, 0);
	nodeVector.push_back(n1);
	node n2(1, 1, 0);
	nodeVector.push_back(n2);
	node n3(0, 0, 0);
	nodeVector.push_back(n3);
	node n4(1, 0, 0);
	nodeVector.push_back(n4);

	triangle t1(1, 2, 3);
	triangleVector.push_back(t1);
	triangle t2(2, 3, 4);
	triangleVector.push_back(t2);
}

mesh::mesh(nlohmann::json meshInput)
{
	nodeVector.reserve(meshInput["nodeNumber"].get<size_t>());
	triangleVector.reserve(meshInput["triangleNumber"].get<size_t>());
	
	for(int i = 0; i < meshInput["nodeNumber"].get<size_t>(); i++)
	{
		//Depiler les nodes un a un
		node currentNode(meshInput["node"][std::to_string(i)]["x"].get<float>(), meshInput["node"][std::to_string(i)]["y"].get<float>(), meshInput["node"][std::to_string(i)]["z"].get<float>());
		nodeVector.push_back(currentNode);
	}

	for(int i = 0; i < meshInput["triangleNumber"].get<size_t>(); i++)
	{
		//Depiler les triangles un a un
		triangle currentTriangle(meshInput["triangle"][std::to_string(i)]["A"].get<int>(), meshInput["triangle"][std::to_string(i)]["B"].get<int>(), meshInput["triangle"][std::to_string(i)]["C"].get<int>());
		triangleVector.push_back(currentTriangle);
	}

}

void mesh::update(nlohmann::json nodeUpdates)
{
	//First, check if my received json have the right amount of nodes

	if(nodeUpdates["nodeNumber"].get<size_t>() == nodeVector.size())
	{
		for(int i = 0; i < nodeVector.size(); i++)
		{
			node currentNode(nodeUpdates["node"][std::to_string(i)]["x"].get<float>(), nodeUpdates["node"][std::to_string(i)]["y"].get<float>(), nodeUpdates["node"][std::to_string(i)]["z"].get<float>());
			nodeVector[i] = currentNode;
		}
	}
	else
	{

		std::stringstream ss;
    	int valReceived = nodeUpdates["nodeNumber"].get<int>();
		ss << "Json node size error : received " << valReceived << "while expecting" << nodeVector.size();
		throw(ss.str());
	}

	//Triangles should stay the same. We only care about node position here
}

nlohmann::json mesh::toJson()
{	
	nlohmann::json retval;
	//parcourir le vecteur de node
	retval["nodeNumber"] = nodeVector.size(); 
	retval["triangleNumber"] = triangleVector.size(); 
		
	int i = 0;
	for(const auto& value: nodeVector)
	{
		retval["node"][std::to_string(i)]["x"] = value.x; 
		retval["node"][std::to_string(i)]["y"] = value.y;
		retval["node"][std::to_string(i)]["z"] = value.z;
		i++;	
	}
	i = 0;
	//parcourir le vecteur de triangle
	for(const auto& value: triangleVector)
	{
		retval["triangle"][std::to_string(i)]["A"] = value.firstPoint; 
		retval["triangle"][std::to_string(i)]["B"] = value.secondPoint;
		retval["triangle"][std::to_string(i)]["C"] = value.thirdPoint;
		i++;	
	}

	return retval;
}



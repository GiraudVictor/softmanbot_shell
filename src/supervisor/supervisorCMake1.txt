find_package(catkin REQUIRED)
find_package(catkin REQUIRED COMPONENTS roscpp rospy)

include_directories(SYSTEM ${Boost_INCLUDE_DIRS})
find_package(Boost REQUIRED COMPONENTS serialization)


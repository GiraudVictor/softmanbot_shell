#include "specificSupervisor.hpp"
#include <ros/ros.h>
#include <iostream>

supervisoryInterface& getSupervisoryInterface(void)
{
	static michelinSupervisoryInterface retval;
	return retval;
}

softmanbotState state_machine(void)
{
		return IDLE;
}

void specificSupervisoryRosInit(void)
{

}

void michelinSupervisoryInterface::supervisoryInit(void)
{
	std::cout << "Call polymorphed version of the interface : Init" << std::endl;
}

void michelinSupervisoryInterface::supervisoryIdle(void)
{
	std::cout << "Call polymorphed version of the interface : Idle" << std::endl;
}

void michelinSupervisoryInterface::supervisoryStop(void)
{
	std::cout << "Call polymorphed version of the interface : Stop" << std::endl;
}

void michelinSupervisoryInterface::supervisoryPerformT1S2(void)
{
	std::cout << "Call polymorphed version of the interface : PerformT1S2" << std::endl;
}

void michelinSupervisoryInterface::supervisoryGraspingT1S3(void)
{
	std::cout << "Call polymorphed version of the interface : GraspingT1S3" << std::endl;
}
void michelinSupervisoryInterface::supervisoryPerformT1S3(void)
{
	std::cout << "Call polymorphed version of the interface : PerformT1S3" << std::endl;
}

include_directories( ${catkin_INCLUDE_DIRS})

add_executable(softmanbot_supervisor_node
	common/genericSupervisor.cpp common/genericSupervisor.hpp common/genericLogic.hpp
	supervisor/specificSupervisor.cpp supervisor/specificSupervisor.hpp
	)

target_link_libraries(softmanbot_supervisor_node
	${catkin_LIBRARIES}
	${Boost_LIBRARIES}
)



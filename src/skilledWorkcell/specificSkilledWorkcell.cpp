#include "specificSkilledWorkcell.hpp"

#include <ros/ros.h>

static nodeID currentMaster = SKILLED_WORKCELL;//Switch this in different state so that

skilledWorkcellInterface& getSkilledWorkcellInterface(void)
{
	static michelinSkilledWorkcellInterface retval;
	return retval;
}

void michelinSkilledWorkcellInterface::skilledWorkcellInit(void)
{
	std::cout << "Call polymorphed version of the interface : Init" << std::endl;
}

void michelinSkilledWorkcellInterface::skilledWorkcellIdle(void)
{
	std::cout << "Call polymorphed version of the interface : Idle" << std::endl;
}

void michelinSkilledWorkcellInterface::skilledWorkcellStop(void)
{
	std::cout << "Call polymorphed version of the interface : Stop" << std::endl;
}

void michelinSkilledWorkcellInterface::skilledWorkcellPerformT1S2(void)
{
		std::cout << "Call polymorphed version of the interface : PerformT1S2" << std::endl;
}

void michelinSkilledWorkcellInterface::skilledWorkcellGraspingT1S3(void)
{
	std::cout << "Call polymorphed version of the interface : GraspingT1S3" << std::endl;
}

void michelinSkilledWorkcellInterface::skilledWorkcellPerformT1S3(void)
{
	std::cout << "Call polymorphed version of the interface : PerformT1S3" << std::endl;
}

nodeID specificSkilledWorkcellGetMaster(void)
{
	return currentMaster;
}

void specificSkilledWorkcellControl(std::string commandString, std::string sensorString)
{
	ROS_INFO("start controlling robot");

	return;
}


void specificSkilledWorkcellRosInit()
{
}

void specificSkilledWorkcell_setSensor(std::string param)
{
	std::string sensorString = param;
	return;
}
